# REL-H5P

## Nom
Ressources Educatives Libres - Essentiellement au format H5P

## Description
Mathématiques - Cycle 3 

Numération - Automatismes - Résolution de Problèmes

## Principaux outils utilisés
Environnement d'exploitation : Débian - Applications : Geogebra - Libre Office - Inkscape - Pencil2D

## Installation
Environnement susceptible d'exploiter une ressource au format H5P.
Par exemple Logiquiz ou Lumi

## Usage


## Support
Compte externe - Sans accès Tchap actuellement.

## Roadmap
Résolution de Problèmes.

## Contributions
Toute contribution est la bienvenue

## Auteurs


## License
GNU General Public License 3

## Project status
Compte externe - Sans accès Tchap actuellement.